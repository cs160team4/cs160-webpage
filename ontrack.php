<?php
	include("dbconnect.php");
	include("CommonLogin.php");

	if ($login_success == 1)
		$username = $_SESSION['username'];
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CS160 | On-Track Learning</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Homepage</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="forums"/>Forums</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Discover Activities<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="browse.php">Browse</a></li>
						<li><a href="search.php">Search</a></li>
						<li><a href="ontrack.php">On-Track Learning</a></li>
                        <li class="divider"></li>
						<li class="dropdown-header">Topic Specific</li>
                        <li><a href="search.php?category=Physics">Physics</a></li>
						<li><a href="search.php?category=Biology">Biology</a></li>
                        <li><a href="search.php?category=Chemistry">Chemistry</a></li>
						<li><a href="search.php?category=Earth Science">Earth Science</a></li>
                        <li><a href="search.php?category=Math">Math</a></li>
					</ul>
				</li>
				<li><a href="about.php">About</a></li>
			</ul>
						<?php
							if ($login_success == 1) {
								echo "<ul class=\"nav navbar-nav\"><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">My Stuff<span class=\"caret\"></span></a><ul class=\"dropdown-menu\" role=\"menu\"><li class=\"dropdown-header\">$username</li>
								<li><a href=\"profile.php\">Your Profile</a></li>
								<li><a href=\"logout.php\">Logout</a></li>
								</ul></li></ul>";
							}
							elseif ($login_success == 0) {
								$_SESSION["fail"] = true;
								echo "<meta http-equiv=\"refresh\" content=\"1;url=login.php\">
								<script type=\"text/javascript\">
								window.location.href = \"login.php\"
								</script>";
							}
						?>
			<?php
          $form = "<form class=\"navbar-form navbar-right\" method=\"post\">
            <div class=\"form-group\">
              <input type=\"email\" placeholder=\"Email\" name=\"email\" class=\"form-control\">
            </div>
            <div class=\"form-group\">
              <input type=\"password\" placeholder=\"Password\" name=\"password\" class=\"form-control\">
            </div>
            <button type=\"submit\" name=\"submit\" class=\"btn btn-danger\">Sign in</button>
			<a href=\"registration.php\"><button type=\"button\" class=\"btn btn-primary\">Register</button></a>
          </form>";
		  
			if ($login_success == 1) {
				echo "<form class=\"navbar navbar-right\"><h3><a href=\"logout.php\"><span class=\"label label-default\">Not $username?</span></a></h3></form>";
			}
			else
				echo $form;
		  ?>
        </div><!--/.navbar-collapse -->
      </div>
	  </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>On-Track Learning</h1>
        <p>Don't know where to start? Pick a grade level, then a topic!</p>
      </div>
    </div>


    <!-- I feel dumb that this is called an 'accordion'. I literally could not google this out for hours >_>... -Carl -->
    
    <div class="container">
      
      
      
      <div class="bs-example">
            <div class="panel-group" id="accordion">
            
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Kindergarten</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">1st Grade</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">2nd Grade</a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">3rd Grade</a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">4th Grade</a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">5th Grade</a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">6th Grade</a>
                        </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">7th Grade</a>
                        </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine">8th Grade</a>
                        </h4>
                    </div>
                    <div id="collapseNine" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen">9th Grade</a>
                        </h4>
                    </div>
                    <div id="collapseTen" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">10th Grade</a>
                        </h4>
                    </div>
                    <div id="collapseEleven" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve">11th Grade</a>
                        </h4>
                    </div>
                    <div id="collapseTwelve" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen">12th Grade</a>
                        </h4>
                    </div>
                    <div id="collapseThirteen" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                              <p>
                                Lorem ipsum nec conubia euismod morbi nisi condimentum, dolor hendrerit integer duis nam purus, vitae dapibus praesent tristique sollicitudin convallis nisi eget convallis sapien aenean phasellus luctus nisi arcu lacinia convallis dictum faucibus.
                                Fames felis mi tempor dui duis augue viverra orci pulvinar, class congue malesuada suspendisse praesent cubilia arcu sapien maecenas augue est accumsan.
                                Rhoncus consequat sociosqu felis tempus curabitur enim, volutpat fermentum lorem quis leo fringilla, habitasse curae nullam maecenas curabitur molestie adipiscing etiam blandit convallis donec turpis maecenas venenatis turpis habitant sodales.
                                Lectus ipsum per inceptos suspendisse eget diam nisl taciti, sagittis torquent sapien nam convallis consequat ornare rhoncus ut, potenti ullamcorper aliquet mollis eleifend ultrices eleifend.
                              </p>
                            </div>
                            <div class="col-md-6">
                              <p><a class="btn btn-primary" href="search.php?category=Physics" role="button">Physics &raquo;</a></p>
                              <p><a class="btn btn-success" href="search.php?category=Biology" role="button">Biology &raquo;</a></p>
                              <p><a class="btn btn-info" href="search.php?category=Chemistry" role="button">Chemistry &raquo;</a></p>
                              <p><a class="btn btn-warning" href="search.php?category=Earth Science" role="button">Earth Science &raquo;</a></p>
                              <p><a class="btn btn-danger" href="search.php?category=Math" role="button">Math &raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
        </div>
      
      <hr>
      <footer>
        <p>&copy; EduPower 2014 - <?php echo date("Y") ?></p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
